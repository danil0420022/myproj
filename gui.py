import os
import table
import sys  # sys нужен для передачи argv в QApplication
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5 import QtSql
from PyQt5.QtWidgets import QApplication, QWidget, QComboBox, QHBoxLayout, QVBoxLayout,QFileDialog
from PyQt5.QtGui import QStandardItem, QStandardItemModel, QFont
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtSql import QSqlTableModel
from create_matrix import black_and_white, get_matrix
from inter_matrix import integral_view



class ExampleApp(QtWidgets.QMainWindow, table.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.getfile()
        reg_numbers = QRegExp("[0-9]+")
        Validator_numbers = QRegExpValidator(self)
        Validator_numbers.setRegExp(reg_numbers)
        self.lineEdit.setValidator(Validator_numbers)
        self.lineEdit_2.setValidator(Validator_numbers)
        self.lineEdit_3.setValidator(Validator_numbers)
        self.lineEdit_4.setValidator(Validator_numbers)
        #button add_pict
        self.add_pict.clicked.connect(self.getfile)
        #button calculate
        self.calculate.clicked.connect(self.area)
    
    #get image
    def getfile(self):
        """Get filename of image, and call other functions."""
        fname = QFileDialog.getOpenFileName(self, 'Open file', 'c:\\',"Image files (*.jpg *.gif *.png)")
        print(fname)
        print(type(fname[0]))
        if fname[0] != '':
            black_and_white(fname)
            img=QPixmap('1.png')
            self.label.setPixmap(img)
            self.arr=get_matrix()
            self.arr_2=list(self.arr)
            self.inter_arr=integral_view(self.arr_2)
            self.matrix.setRowCount(len(self.arr))
            self.matrix.setColumnCount(len(self.arr[0]))
            self.i_matrix.setRowCount(len(self.inter_arr))
            self.i_matrix.setColumnCount(len(self.inter_arr[0]))
            for i,row in enumerate(self.arr):
                for j,val in enumerate(row):
                    self.matrix.setItem(i,j,QtWidgets.QTableWidgetItem(str(val)))
            for i,row in enumerate(self.inter_arr):
                for j,val in enumerate(row):
                    self.i_matrix.setItem(i,j,QtWidgets.QTableWidgetItem(str(val)))
    def area(self):
        """Calculate summ of elements on chosen matrox area."""
        x=(self.lineEdit.text())
        y=(self.lineEdit_2.text())
        x1=(self.lineEdit_3.text())
        y1=(self.lineEdit_4.text())
        problem=False 
        if y =='' or int(y) > 250 or int(y) ==0:
            self.trouble()
            problem=True
        elif x=='' or int(x) > 300 or int(x)==0 :
            self.trouble()
            problem=True
        elif x1=='' or int(x1) > 300 or  int(x1) < int(x):
            self.trouble()
            problem=True
        elif y1=='' or int(y1) > 250 or int(y1) <int(y):
            self.trouble()
            problem=True
        elif  x==1 and y==1:
            x=int(x)
            y=int(y)
            x1=int(x1)
            y1=int(y1)
            area=self.inter_arr[y1-1][x1-1]
        elif x==1:
            x=int(x)
            y=int(y)
            x1=int(x1)
            y1=int(y1)
            area=self.inter_arr[y1-1][x1-1]-self.inter_arr[y-2][x1-1]
        elif y==1:
            x=int(x)
            y=int(y)
            x1=int(x1)
            y1=int(y1)
            area=self.inter_arr[y1-1][x1-1]-self.inter_arr[y1-1][x-2]
        else:
            x=int(x)
            y=int(y)
            x1=int(x1)
            y1=int(y1)
            area=self.inter_arr[y-2][x-2]+self.inter_arr[y1-1][x1-1]-self.inter_arr[y-2][x1-1]-self.inter_arr[y1-1][x-2]
        if problem == False:
            text=f'Сумма выбранных объетов матрицы равна: {area}'
            self.listWidget.clear()
            self.listWidget.addItem(text)
    
    
    def trouble(self):
        """Call error mesage box.""" 
        self.lineEdit.clear()
        self.lineEdit_2.clear()
        self.lineEdit_3.clear()
        self.lineEdit_4.clear()
        self.total_text = ("Введите координаты, не превышающие размеры матрицы, а также, чтобы координаты правого нижнего края области (x1,y1) не были меньше координат верхнего левого края (x,y).")
        self.msg = QtWidgets.QMessageBox()
        self.msg.setIcon(QtWidgets.QMessageBox.Warning)
        self.msg.setText(self.total_text)
        self.msg.exec_()

        