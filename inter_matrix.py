import numpy as np
from create_matrix import get_matrix
def integral_view(image: list) -> list:
    """"Convert matrix to inter matrix."""
    inter_arr = []
    for i in range(len(image)):
        inter_arr.append([])
        for w in range(len(image[i])):
            inter_arr[i].append(sum(i, w, image, inter_arr))
    return np.array(inter_arr)


def sum(y, x, array, inter_arr):
    total = 0
    if x == 0 and y == 0:
        temp=int(array[y][x])
        total = array[y][x]
    elif x == 0:
        temp=int(array[y][x])
        total = inter_arr[y-1][x]+temp
    elif y == 0:
        temp=int(array[y][x])
        now_temp=inter_arr[y][x-1]
        total = now_temp+temp
    else:
        temp=int(array[y][x])
        total = inter_arr[y][x-1]+inter_arr[y-1][x] + \
            temp-inter_arr[y-1][x-1]
    return total

