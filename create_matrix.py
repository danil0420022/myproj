import numpy as np
from numpy import array
from PIL import Image 
import numpy.random as rand
def get_matrix():
    """Convert image to matrix."""
    img=Image.open('1.png')
    arr=array(img)
    return arr

def black_and_white(img):
    """Make image in black and white style"""
    n_i= Image.open(img[0])
    image_file=n_i.resize((300, 250))
    thresh = 200
    fn = lambda x : 255 if x > thresh else 0
    image_file = image_file.convert('L').point(fn)
    image_file.save('1.png')

